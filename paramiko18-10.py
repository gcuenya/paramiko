import paramiko
import os

import subprocess

#os.system("C:\\Program Files\\CONAE\\SInterCL\\"+" "+"TASKKILL/F/IM Sinter.exe")
#print("Sinter cerrado")

####################################################################################################
# Inicia un cliente SSH
ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect('192.168.11.172', 2222, "Administrator" , "1234" )
####################################################################################################
###matar procesos del bridge
entrada, salida, error = ssh_client.exec_command('cmd /c tskill /A T-FBKBRGTCPP1A01')
print("Bridge cerrado")
####################################################################################################
####borra el archivo de binarios
entrada, salida, error = ssh_client.exec_command('cmd /c del "D:\\SAO\\App\\T-FBKBRGTCPP1A01\\bin\\2019-10-18_TCReceivedFrame_0.bin"')
####################################################################################################
###ejecuto el bridge
entrada, salida, error = ssh_client.exec_command("C:\\Users\\Administrator\Desktop\\sinter_test.bat")
####################################################################################################
###############ejecuto el Sinter
FNULL = open(os.devnull, 'w')
args = "C:\\Program Files\\CONAE\\SInterCL\\SInterCL.exe" + ' -m ' +  "Test_SME::sme_encode"
subprocess.call(args, stdout=FNULL, stderr=FNULL, shell=False)
print("se ejecuta el archivo SCL")
###################################################################################################
#os.system("scp 2019-10-17_TCReceivedFrame_0.bin Administratror@192.168.11.172:D:\\SAO\\App\\T-FBKBRGTCPP1A01\\bin") #eg os.system("scp foo.bar joe@srvr.net:/path/to/foo.bar")


sftp = ssh_client.open_sftp()
sftp.get('SAO\\App\\T-FBKBRGTCPP1A01\\bin\\2019-10-18_TCReceivedFrame_0.bin','D:\\a.bin')

sftp.close()


# Cerrar la conexión
ssh_client.close()
